<?php

namespace HostGuestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reservation
 *
 * @ORM\Table(name="Reservation", indexes={@ORM\Index(name="confirmation", columns={"confirmation"}), @ORM\Index(name="id_log", columns={"id_log"})})
 * @ORM\Entity(repositoryClass="HostGuestBundle\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Reservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_res", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRes;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_voyageur", type="integer", nullable=false)*
     * @Assert\NotBlank(message = "Merci de taper un nombre ")
     */
    private $nbVoyageur;

    /**
     * @var $confirmation
     * @ORM\Column(name="confirmation", type="boolean", options={"default":false})
     */
    private $confirmation;

    /**
     * @ORM\Column(name="num_tel", type="string", length=255)
     * @Assert\Regex(
     *     pattern     = "/[a-zA-Z]/",
     *     match=false,
     *     message="ton numéro doit contenir seulement des chiffres"
     * )
     * @Assert\Length(
     *      min = 11,
     *      max = 15,
     *      minMessage = " ton numéro de téléphone doit contenir au moins {{ limit }} chiffres",
     *      maxMessage = "ton numéro de téléphone doit contenir au maximum {{ limit }} chiffres"
     * )
     * @Assert\NotBlank(message = "Merci de taper votre numéro de Téléphone")
     */
    private $numTel;

    /**
     * @ORM\Column(type="string",length=255)
     */

    private $civilite;

    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper votre Nom")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre Nom ne doit pas contenir aucun chiffre"
     * )
     */

    private $nom;

    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper votre Prénom")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Votre prénom ne doit pas contenir aucun chiffre"
     * )
     */
    private $prenom;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "Merci de vérifier le format de votre adresse mail",
     *     checkMX = true
     * )
     * @Assert\NotBlank(message = "Merci de taper votre e-mail")
     */


    private $email;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $pays;




    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper votre password")
     * @Assert\Length(
     *      min = 4,
     *      max = 12,
     *      minMessage = "votre mot de passe doit contenir au moins {{ limit }} caractéres",
     *      maxMessage = "votre mot de passe doit contenir au maximum {{ limit }} caractéres"
     * )
     */
    private $motdepasse;

    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper votre Message")
     */
    private $message;
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper le numéro de votre carte")
     * @Assert\Length(
     *      min = 16,
     *      max = 16,
     *      exactMessage = "Merci d'introduire les 16 numéros de votre carte"
     * )
     * @Assert\Regex(
     *     pattern     = "/[a-zA-Z]/",
     *     match=false,
     *     message="votre numéro de carte doit contenir seulement des chiffres"
     * )
     */
    private $numcarte;

    /**
     * @ORM\Column(type="date")
     * @Assert\GreaterThan(value ="today",message="Vote carte est expirée,Merci de vérifier")
     */
    private $dateexpirationcarte;
    /**
     * @ORM\Column(name="expiredate", type="datetime", nullable=true)
     *
     */
    private $expiredate;
    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="idProp",referencedColumnName="id",onDelete="CASCADE"  )
     */
    private $proprietaire;
    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\logement")
     * @ORM\JoinColumn(name="id_log",referencedColumnName="id_log" )
     */
    private $idLog;
    /**
     * @ORM\Column(type="string",length=255)
     * @Assert\NotBlank(message = "Merci de taper votre Adresse")
     * @Assert\Length(
     *      min = 10,
     *      minMessage = "votre adresse doit contenir au moins{{ limit }} caractéres"
     * )
     */

    private $Adresse;
    /**
     * @ORM\Column(type="string",length=255)
     */

    private $typecarte;


    /**
     * @return mixed
     */
    public function getProprietl()
    {
        return $this->proprietl;
    }

    /**
     * @param mixed $proprietl
     */
    public function setProprietl($proprietl)
    {
        $this->proprietl = $proprietl;
    }


    /**
     * @return mixed
     */
    public function getconfirmation()
    {
        return $this->confirmation;
    }

    public function setconfirmation($confirmation)
    {
        $this->confirmation = $confirmation;
    }

    /**
     * @return mixed
     */
    public function getDateexpirationcarte()
    {
        return $this->dateexpirationcarte;
    }

    /**
     * @param mixed $dateexpirationcarte
     */
    public function setDateexpirationcarte($dateexpirationcarte)
    {
        $this->dateexpirationcarte = $dateexpirationcarte;
    }

    public function __toString()
    {
        return '1';
    }

    /**
     * @return mixed
     */
    public function getidRes()
    {
        return $this->idRes;
    }

    /**
     * @param mixed $id
     */
    public function setidRes($idRes)
    {
        $this->id = $idRes;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getcivilite()
    {
        return $this->civilite;
    }

    /**
     * @param mixed $civilite
     */
    public function setcivilite($civilite)
    {
        $this->civilite = $civilite;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getpays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setpays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return mixed
     */
    public function getnumTel()
    {
        return $this->numTel;
    }

    /**
     * @param mixed $pays
     */
    public function setnumTel($numTel)
    {
        $this->numTel = $numTel;
    }

    /**
     * @return mixed
     */
    public function getMotdepasse()
    {
        return $this->motdepasse;
    }

    /**
     * @param mixed $motdepasse
     */
    public function setMotdepasse($motdepasse)
    {
        $this->motdepasse = $motdepasse;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getNumcarte()
    {
        return $this->numcarte;
    }

    /**
     * @param mixed $numcarte
     */
    public function setNumcarte($numcarte)
    {
        $this->numcarte = $numcarte;
    }

    /**
     * @return int
     */
    public function getNbVoyageur()
    {
        return $this->nbVoyageur;
    }

    /**
     * @param int $nbVoyageur
     */
    public function setNbVoyageur($nbVoyageur)
    {
        $this->nbVoyageur = $nbVoyageur;
    }

    /**
     * @return \Logement
     */
    public function getIdLog()
    {
        return $this->idLog;
    }

    /**
     * @param \Logement $idLog
     */
    public function setIdLog($idLog)
    {
        $this->idLog = $idLog;
    }

    /**
     * @return mixed
     */
    public function getexpiredate()
    {
        return $this->expiredate;
    }

    /**
     * @param mixed $expiredate
     */
    public function setexpiredate($expiredate)
    {
        $this->expiredate = $expiredate;
    }

    /**
     * @return mixed
     */
    public function getProprietaire()
    {
        return $this->proprietaire;
    }

    /**
     * @param mixed $proprietaire
     */
    public function setProprietaire($proprietaire)
    {
        $this->proprietaire = $proprietaire;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->Adresse;
    }

    /**
     * @param mixed $Adresse
     */
    public function setAdresse($Adresse)
    {
        $this->Adresse = $Adresse;
    }

    /**
     * @return mixed
     */
    public function gettypecarte()
    {
        return $this->typecarte;
    }

    /**
     * @param mixed $typecarte
     */
    public function settypecarte($typecarte)
    {
        $this->typecarte = $typecarte;
    }
}

