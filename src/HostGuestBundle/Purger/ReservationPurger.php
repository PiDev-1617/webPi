<?php
// src/reservBundle/Purger/ReservationPurger.php

namespace HostGuestBundle\Purger;

use Doctrine\ORM\EntityManagerInterface;

class ReservationPurger
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    // On injecte l'EntityManager
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function purge($days)
    {
        // date d'il y a $days jours
        $date = new \Datetime($days . ' days ago');

        // On récupère les réservations à supprimer
        $listAdverts = $this->em->getRepository('HostGuestBundle:Reservation')->getreservationBefore($date);

        // On parcourt les reservations pour les supprimer effectivement
        foreach ($listAdverts as $advert) {
            $this->em->remove($advert);
        }

        // Et on n'oublie pas de faire un flush !
        $this->em->flush();
    }
}
