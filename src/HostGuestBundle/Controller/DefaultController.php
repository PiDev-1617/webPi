<?php

namespace HostGuestBundle\Controller;

use HostGuestBundle\Entity\Reservation;
use HostGuestBundle\Entity\Supreser;
use HostGuestBundle\Form\ReservationType;
use HostGuestBundle\Form\SupReservationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function purgeAction(Request $request)
    {
        // On récupère notre service//config/services
        $purger = $this->get('hostguest.reservation_purger');
//maryem
        // On purge les réservations
        $purger->purge(3);//effacer les reservations qui ne sont pas confirmés pendant 3jrs

        // On ajoute un message flash arbitraire

        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Félicitations, les réservations plus vieilles que 3 jours ont été purgées ');
        // On redirige vers la page d'accueil
        return $this->redirect($this->generateUrl('hostguest_management'));//?!
    }

    public function listreservationsAction()
    {
        $logement = $this->getDoctrine()
            ->getRepository('AppBundle:logement')//récupérer l'entité logement
            ->findall();

        $annonce = $this->getDoctrine()
            ->getRepository('HostGuestBundle:Reservation')//récupérer l'entité reservation
            ->findall();

        return $this->render('HostGuestBundle:Default:listreservation.html.twig', ['annonce' => $annonce]);
    }


    public function AjouterReservationAction(Request $request, $id)
    {

        $reserv = new Reservation();//instanciation de l'entité réservation
        $em = $this->getDoctrine()->getManager();// Accés à l'entité Resrvation
        $user = $this->getUser();//
        $pro = $em->getRepository('piUserBundle:User')->find($user);//accés à l'entité User
        $logement = $em->getRepository('AppBundle:logement')->find($id);//Accés à l'entité logement à travers getRepository

        $form = $this->createForm(ReservationType::class, $reserv);


        if ($form->handleRequest($request)->isValid()) {
            $reserv->setconfirmation(false);
            $reserv->setIdLog($logement);
            $reserv->setProprietaire($pro);
            $date = new \DateTime();
            $reserv->setexpiredate($date);
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserv);
            $em->flush();
            $idl = $reserv->getidRes();

            $this->get('ras_flash_alert.alert_reporter')->addSuccess("Félicitations votre réservation a été efectué avec succes.Vous devez le confirmer durant 3 jours ou elle sera annulé de façon automatique");
            return $this->redirectToRoute('hostguest_reservation_3', array('id' => $idl, 'idl' => $id));
        }
        return $this->render('HostGuestBundle:Default:reservation.html.twig', array('form' => $form->createView(), 'annonce' => $logement));




    }


    function AfficherReservationAction(Request $request, $id, $idl)
    {

        $logement = $this->getDoctrine()->getRepository('AppBundle:logement')->find($idl);//récupérer l'entité logement
        $reservation = $this->getDoctrine()->getRepository('HostGuestBundle:Reservation')->find($id);//récupérer l'entité reservation


        return $this->render('HostGuestBundle:Default:confirmation.html.twig', array('reservation' => $reservation, 'logement' => $logement));
        // afficher les coordonnés du membre qui a réservé
    }

    function confirmerReservationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $reservation = $em->getRepository('HostGuestBundle:Reservation')->find($id);
        $reservation->setconfirmation(true);
        $em->persist($reservation);
        $em->flush();
        $request->getSession()
            ->getFlashBag()
            ->add('success', 'Félicitations, Votre réservation a été comfirmeé avec succes ');
        return $this->redirectToRoute('hostguest_management');

    }

    public function managementReserAction()
    {
        $user = $this->getUser();//accés à l'entité user

        $iduser = $user->getid();
        $reservation = $this->getDoctrine()->getRepository('HostGuestBundle:Reservation')->getreservation($iduser);

        //$logement= $this->getDoctrine()->getRepository('AppBundle:logement')->getlogement($reservation->getidLog()->getidLog());

        if ($reservation == null) {
            $this->get('ras_flash_alert.alert_reporter')->addError("Merci de noter que vous n'avez pas aucune réservation dans notre site ");
        } else {
            $dt = '';
            foreach ($reservation as $ad) {
                $logement = $this->getDoctrine()->getRepository('AppBundle:logement')->getlogement($ad->getidLog()->getidLog());
                foreach ($logement as $as) {
                    $type = $as->getTypeChambre();
                    $na = $as->getTypeHabitation();
                    $id = $ad->getidRes();
                    $dt = $id . '(' . $type . ',' . $na . ')  ' . $dt;
                }

            }

            $this->get('ras_flash_alert.alert_reporter')->addInfo("Merci de noter que votre(vos) réservation(s) est(sont) enregistré(s) sous le(s) numéro(s):   " . $dt);
        }

        return $this->render('HostGuestBundle:Default:annu_mod.html.twig');
    }

    function SupprimerAction()
    {


        $reserv = new Supreser();//instanciation de l'entité supreser qui contient l'id de reservation et le mot de passe
        $form = $this->createForm(SupReservationType::class, $reserv);
        return $this->render(
            'HostGuestBundle:Default:supp.html.twig',
            array('form' => $form->createView())
        );

    }

    function AfficherAction()
    {


        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        return $this->render(
            'HostGuestBundle:Default:mod.html.twig',
            array('form' => $form->createView())
        );

    }

    function GenererPDFAction()
    {


        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        return $this->render(
            'HostGuestBundle:Default:pdf.html.twig',
            array('form' => $form->createView())
        );

    }

    function ConfimerAction()
    {


        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        return $this->render(
            'HostGuestBundle:Default:conf.html.twig',
            array('form' => $form->createView())
        );

    }

    function ConfimerTraitementAction(Request $request)
    {
        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        $form->handleRequest($request);
        $data = $form->getData();
        $maryem = $data->getId();
        $pass = $data->getmotdepasse();
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository('HostGuestBundle:Reservation')->getreservationsu($maryem, $pass);

        if ($model == null) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', ' Merci de vérifier les coordonnées de votre réservation ');
            return $this->redirectToRoute('hostguest_management');// si l'id ou le mot de passe n'existent pas dans la BD donc pas de passage à l'interface de confirmation

        } else {
            foreach ($model as $am) {


                $idr = $am->getidRes();

                $id = $am->getidLog()->getidLog();

                return $this->redirectToRoute('hostguest_reservation_3', array('id' => $idr, 'idl' => $id));
            }
        }

    }

    public function mypdfAction(Request $request)
    {
        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        $form->handleRequest($request);
        $data = $form->getData();
        $maryem = $data->getId();
        $pass = $data->getmotdepasse();
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository('HostGuestBundle:Reservation')->getreservationpdf($maryem, $pass);
        if ($model == null) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', ' Merci de vérifier les coordonnées de votre réservation ou de la confirmer');
            return $this->redirectToRoute('hostguest_management');

        } else {
            foreach ($model as $am) {


                $idr = $am->getidRes();

                $id = $am->getidLog()->getidLog();

                $maryem = $em->getRepository('AppBundle:logement')->getlogement($id);


        $pdf = $this->container->get("white_october.tcpdf")->create(
            'LANDSCAPE',
            PDF_UNIT,
            PDF_PAGE_FORMAT,
            true,
            'UTF-8',
            false
        );
        $pdf->SetAuthor('qweqwe');
        $pdf->SetTitle('HostandGuest PDF');
        $pdf->SetSubject('Your client');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->setFontSubsetting(true);
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);
                $pdf->setCellHeightRatio(1);

                $pdf->SetFont('helvetica', '', 5, '', true);
        $pdf->AddPage();

                $html = $this->renderView('HostGuestBundle:Default:pdftemplate.html.twig', array('model' => $am));

                $pdf->writeHTML($html);


        $pdf->Output("Reservation.pdf", 'I');
            }
        }
    }

    function AfficherTraitementAction(Request $request)
    {
        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        $form->handleRequest($request);
        $data = $form->getData();

        $maryem = $data->getId();
        $pass = $data->getmotdepasse();
        $em = $this->getDoctrine()->getManager();
        $am = $em->getRepository('HostGuestBundle:Reservation')->getreservationsu($maryem, $pass);
        if ($am == null) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', ' Merci de vérifier les coordonnées de votre réservation ');
            return $this->redirectToRoute('hostguest_management');

        } else {
            foreach ($am as $model) {


                $idr = $model->getidRes();

                $id = $model->getidLog()->getidLog();

                $reserv1 = new Reservation();

                $form1 = $this->createForm(ReservationType::class, $reserv1);

                return $this->render(
                    'HostGuestBundle:Default:mod_affich.html.twig',
                    array('form' => $form1->createView(), 'model' => $model, 'id' => $id)
                );

            }
        }
    }

    public function ModifyReservationAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $model = $em->getRepository('HostGuestBundle:Reservation')->find($id);

        $form = $this->createForm(ReservationType::class, $model);

        if ($form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($model);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Félicitations, Votre réservation a été modifiée avec succes ');

            return $this->redirectToRoute('hostguest_management');
        }

        return $this->render(
            'HostGuestBundle:Default:mod_affich.html.twig',
            array('form' => $form->createView(), 'model' => $model, 'id' => $id)
        );


    }

    function SupprimerTraitementAction(Request $request)
    {
        $reserv = new Supreser();
        $form = $this->createForm(SupReservationType::class, $reserv);
        $form->handleRequest($request);
        $data = $form->getData();
        $maryem = $data->getId();
        $pass = $data->getmotdepasse();
        $em = $this->getDoctrine()->getManager();
        $am = $em->getRepository('HostGuestBundle:Reservation')->getreservationsu($maryem, $pass);
        if ($am == null) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', ' Merci de vérifier les coordonnées de votre réservation ');
            return $this->redirectToRoute('hostguest_management');

        } else {
            foreach ($am as $model) {


                $em->remove($model);
                $em->flush();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Félicitations, Votre réservation a été annuleé avec succes ');
                return $this->redirectToRoute('hostguest_management');
            }
        }


    }
}
