<?php

namespace HostGuestBundle\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 11/02/2017
 * Time: 13:40
 */
class ReservationRepository extends EntityRepository
{
    public function getreservationBefore($date)
    {
        return $this->createQueryBuilder('a')
            ->where('a.expiredate <= :date')// Date de modification antérieure à :date
            ->andWhere('a.confirmation=false')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult();
    }

    public function getreservationsu($id, $md)
    {
        return $this->createQueryBuilder('a')
            ->where('a.idRes = :id')// Date de modification antérieure à :date
            ->andWhere('a.motdepasse=:md')
            ->setParameter('id', $id)
            ->setParameter('md', $md)
            ->getQuery()
            ->getResult();
    }

    public function getreservationpdf($id, $md)
    {
        return $this->createQueryBuilder('a')
            ->where('a.idRes = :id')// Date de modification antérieure à :date
            ->andWhere('a.motdepasse=:md')
            ->andWhere('a.confirmation=true')
            ->setParameter('id', $id)
            ->setParameter('md', $md)
            ->getQuery()
            ->getResult();
    }

    public function getreservation($id)
    {
        return $this->createQueryBuilder('a')
            ->where('a.proprietaire = :id')// Date de modification antérieure à :date
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}