<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventParticipation
 *
 * @ORM\Table(name="Participation")
 * @ORM\Entity
 */
class EventParticipation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */

    private $idP;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Evenement",cascade={"persist"})
     * @ORM\JoinColumn(name="id_ev", referencedColumnName="id_Ev")
     */
    private $evenement;

    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     */
    private $participant;

    /**
     * @ORM\Column(type="string",length=255,columnDefinition="enum('En_Attente', 'acceptee','refusee','annulee')")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateParticipation;

    /**
     * EventParticipation constructor.
     */
    public function __construct()
    {
        $this->DateParticipation = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getIdP()
    {
        return $this->idP;
    }

    /**
     * @param mixed $idP
     */
    public function setIdP($idP)
    {
        $this->idP = $idP;
    }

    /**
     * @return mixed
     */
    public function getEvenement()
    {
        return $this->evenement;
    }

    /**
     * @param mixed $evenement
     */
    public function setEvenement($evenement)
    {
        $this->evenement = $evenement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getParticipant()
    {
        return $this->participant;

    }

    /**
     * @param mixed $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateParticipation()
    {
        return $this->DateParticipation;
    }

    /**
     * @param mixed $DateParticipation
     */
    public function setDateParticipation($DateParticipation)
    {
        $this->DateParticipation = $DateParticipation;
        return $this;
    }


}