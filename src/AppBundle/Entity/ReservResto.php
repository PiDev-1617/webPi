<?php
/**
 * Created by PhpStorm.
 * User: Kossayy
 * Date: 15/02/2017
 * Time: 17:16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * ReservResto
 *
 * @ORM\Table(name="reserv_resto")
 * @ORM\Entity(repositoryClass="pi\EspaceBundle\Repository\espaceRepository")
 */
class ReservResto
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id_rez", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRez;


    /**
     * @var string
     *
     * @ORM\Column(name="req_rez", type="string", length=255, nullable=true)
     */
    private $Requirement;


    /**
     * @var \DateTime
     * @Assert\Range(
     *      min = "now"
     * )
     *
     * @ORM\Column(name="debut_rez", type="date", nullable=false)
     */

    private $Debutrez;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin_rez", type="date", nullable=false)
     * @Assert\Expression(
     *     "this.getFinrez() > this.getDebutrez()",
     *     message="La date fin de l'evenement  doit etre supérieure à la date début"
     * )
     */
    private $Finrez;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbp_rez", type="integer", nullable=false)
     */
    private $NbrePers;

    /**
     * @var integer
     *
     * @ORM\Column(name="repas_rez", type="integer", nullable=false)
     */
    private $nbrrp;
    /**
     * @var string
     *
     * @ORM\Column(name="Offre", type="array", length=255, nullable=true)
     */
    private $remiserez;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EspaceAprox")
     * @ORM\JoinColumn(name="id_resto",referencedColumnName="id_espace",onDelete="CASCADE")
     */
    private $resto;
    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="id_client",referencedColumnName="id",onDelete="CASCADE")
     */
    private $Client;

    /**
     * @var integer
     *
     * @ORM\Column(name="statut_rez", type="integer", nullable=false)
     */
    private $Rstatut;


    /**
     * @return mixed
     */
    public function getLogrez()
    {
        return $this->logrez;
    }

    /**
     * @param mixed $logrez
     */
    public function setLogrez($logrez)
    {
        $this->logrez = $logrez;
    }

    /**
     * @return mixed
     */


    /**
     * @return mixed
     */
    public function getResto()
    {
        return $this->resto;
    }

    /**
     * @param mixed $resto
     */
    public function setResto($resto)
    {
        $this->resto = $resto;
    }


    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->Client;
    }

    /**
     * @param mixed $Client
     */
    public function setClient($Client)
    {
        $this->Client = $Client;
    }

    /**
     * @return int
     */
    public function getIdRez()
    {
        return $this->idRez;
    }

    /**
     * @param int $idRez
     */
    public function setIdRez($idRez)
    {
        $this->idRez = $idRez;
    }

    /**
     * @return string
     */
    public function getRequirement()
    {
        return $this->Requirement;
    }

    /**
     * @param string $Requirement
     */
    public function setRequirement($Requirement)
    {
        $this->Requirement = $Requirement;
    }

    /**
     * @return \DateTime
     */
    public function getDebutrez()
    {
        return $this->Debutrez;
    }

    /**
     * @param \DateTime $Debutrez
     */
    public function setDebutrez($Debutrez)
    {
        $this->Debutrez = $Debutrez;
    }

    /**
     * @return \DateTime
     */
    public function getFinrez()
    {
        return $this->Finrez;
    }

    /**
     * @param \DateTime $Finrez
     */
    public function setFinrez($Finrez)
    {
        $this->Finrez = $Finrez;
    }

    /**
     * @return int
     */
    public function getNbrePers()
    {
        return $this->NbrePers;
    }

    /**
     * @param int $NbrePers
     */
    public function setNbrePers($NbrePers)
    {
        $this->NbrePers = $NbrePers;
    }

    /**
     * @return int
     */
    public function getNbrrp()
    {
        return $this->nbrrp;
    }

    /**
     * @param int $nbrrp
     */
    public function setNbrrp($nbrrp)
    {
        $this->nbrrp = $nbrrp;
    }

    /**
     * @return mixed
     */
    public function getRemiserez()
    {
        return $this->remiserez;
    }

    /**
     * @param mixed $remiserez
     */
    public function setRemiserez($remiserez)
    {
        $this->remiserez = $remiserez;
    }

    /**
     * @return int
     */
    public function getRstatut()
    {
        return $this->Rstatut;
    }

    /**
     * @param int $Rstatut
     */
    public function setRstatut($Rstatut)
    {
        $this->Rstatut = $Rstatut;
    }


}