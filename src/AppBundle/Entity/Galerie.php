<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Galerie
 *
 * @ORM\Table(name="galerie")
 * @ORM\Entity
 */
class Galerie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_gal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGal;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="text", length=65535, nullable=false)
     */
    private $file;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=false)
     */
    private $name;

    /**
     * @return int
     */
    public function getIdGal()
    {
        return $this->idGal;
    }

    /**
     * @param int $idGal
     */
    public function setIdGal($idGal)
    {
        $this->idGal = $idGal;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $pathPhoto
     */
    public function setFile($pathPhoto)
    {
        $this->file = $pathPhoto;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}

