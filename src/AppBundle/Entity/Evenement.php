<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Evenement
 *
 * @ORM\Table(name="evenement")
 * @ORM\Entity(repositoryClass="pi\EventBundle\Repository\EvenementRepository")
 */
class Evenement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_Ev", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEv;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_event", type="string", length=255, nullable=false)
     */
    private $NomEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_event", type="string", length=255, nullable=false)
     */
    private $CategorieEvent;


    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=false)
     */
    private $ville;





    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=false)
     */
    private $dateDebut;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=false)
     */
    private $dateFin;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaire", type="time", nullable=false)
     */
    private $Horaire;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;
    /**
     * @var integer
     *
     * @ORM\Column(name="Places_disponibles", type="integer", nullable=false)
     */
    private $TicketsDispo;
    /**
     * @var float
     *
     * @ORM\Column(name="prix_pass", type="float", precision=10, scale=0, nullable=false)
     */
    private $prixPass;
    /**
     * @var string
     *
     * @ORM\Column(name="lien_Fb", type="text", length=65535, nullable=false)
     */
    private $lienFb;
    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="text", length=65535, nullable=false)
     */
    private $Email;
    /**
     * @var string
     *
     * @ORM\Column(name="list_Contact", type="text", length=65535, nullable=false)
     */
    private $NumTel;
    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="id_organisateur",referencedColumnName="id",onDelete="CASCADE")
     */
    private $Organisateur;

    public function __construct()
    {
        $this->dateDebut = new \DateTimeImmutable();
        $this->dateFin = new \DateTimeImmutable();
    }

    /**
     * @return int
     */
    public function getIdEv()
    {
        return $this->idEv;
    }

    /**
     * @param int $idEv
     */
    public function setIdEv($idEv)
    {
        $this->idEv = $idEv;
    }

    /**
     * @return string
     */
    public function getNomEvent()
    {
        return $this->NomEvent;
    }

    /**
     * @param string $NomEvent
     */
    public function setNomEvent($NomEvent)
    {
        $this->NomEvent = $NomEvent;
    }

    /**
     * @return string
     */
    public function getCategorieEvent()
    {
        return $this->CategorieEvent;
    }

    /**
     * @param string $CategorieEvent
     */
    public function setCategorieEvent($CategorieEvent)
    {
        $this->CategorieEvent = $CategorieEvent;
    }


    /**
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * @param \DateTime $dateDebut
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param \DateTime $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return \DateTime
     */
    public function getHoraire()
    {
        return $this->Horaire;
    }

    /**
     * @param \DateTime $Horaire
     */
    public function setHoraire($Horaire)
    {
        $this->Horaire = $Horaire;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getTicketsDispo()
    {
        return $this->TicketsDispo;
    }

    /**
     * @param int $TicketsDispo
     */
    public function setTicketsDispo($TicketsDispo)
    {
        $this->TicketsDispo = $TicketsDispo;
    }

    /**
     * @return float
     */
    public function getPrixPass()
    {
        return $this->prixPass;
    }

    /**
     * @param float $prixPass
     */
    public function setPrixPass($prixPass)
    {
        $this->prixPass = $prixPass;
    }

    /**
     * @return string
     */
    public function getLienFb()
    {
        return $this->lienFb;
    }

    /**
     * @param string $lienFb
     */
    public function setLienFb($lienFb)
    {
        $this->lienFb = $lienFb;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @param string $Email
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;
    }

    /**
     * @return string
     */
    public function getNumTel()
    {
        return $this->NumTel;
    }

    /**
     * @param string $NumTél
     */
    public function setNumTel($NumTel)
    {
        $this->NumTel = $NumTel;
    }

    /**
     * @return mixed
     */
    public function getOrganisateur()
    {
        return $this->Organisateur;
    }

    /**
     * @param mixed $Organisateur
     */
    public function setOrganisateur($Organisateur)
    {
        $this->Organisateur = $Organisateur;
    }


}

