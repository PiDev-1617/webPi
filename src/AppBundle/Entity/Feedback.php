<?php
/**
 * Created by PhpStorm.
 * User: Mounq
 * Date: 5/14/2017
 * Time: 1:54 PM
 */

/**
 * Created by PhpStorm.
 * User: Mounq
 * Date: 2/13/2017
 * Time: 10:33 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Feedback
{
    /**
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column(type = "integer")
     */
    private $id;
    /**
     * @ORM\Column(type = "text",length=255)
     */
    private $message;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFeedback", type="date", nullable=false)
     */
    private $Date;
    /**
     *
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\logement")
     * @ORM\JoinColumn(name="log_id", referencedColumnName="id_log")
     */
    private $log;
    /**
     * @ORM\Column(type = "integer")
     */
    private $rating;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->Date;
    }

    /**
     * @param \DateTime $Date
     */
    public function setDate($Date)
    {
        $this->Date = $Date;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log)
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }
}



