<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Logement
 *
 * @ORM\Table(name="logement")
 * @ORM\Entity(repositoryClass="pi\logementBundle\Repository\logementRepository")
 */
class logement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLog;
    /**
     * @var string
     *
     * @ORM\Column(name="type_chambre", type="string", length=255, nullable=false)
     */
    private $typeChambre;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;
    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Range(
     *     min = 1,
     *     minMessage = "min 1"
     * )
     * @ORM\Column(name="nb_voyageur", type="integer", nullable=false)
     */
    private $nbVoyageur;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="type_habitation", type="string", length=255, nullable=false)
     */
    private $typeHabitation;
    /**
     * @var integer
     * @Assert\Range(
     *     min = 1,
     *     minMessage = "min 1"
     * )
     * @ORM\Column(name="nb_lit", type="integer", nullable=false)
     */
    private $nbLit;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="type_lit", type="string", length=255, nullable=false)
     */
    private $typeLit;
    /**
     * @var integer
     * @Assert\Range(
     *     min = 1,
     *     minMessage = "min 1"
     * )
     * @ORM\Column(name="nb_SalleBain", type="integer", nullable=false)
     */
    private $nbSallebain;
    /**
     * @var string
     *
     * @ORM\Column(name="type_SB", type="string", length=255, nullable=false)
     */
    private $typeSb;
    /**
     * @var string
     * @ORM\Column(name="adressgmap", type="string", length=255)
     */
    private $adressgmap;
    /**
     * @var string
     *
     * @ORM\Column(name="latlng", type="string", length=255)
     */
    private $latlng;
    /**
     * @var string
     *
     * @ORM\Column(name="equipement", type="array", nullable=true)
     */
    private $equipement;
    /**
     * @var string
     *
     * @ORM\Column(name="espace_utilise", type="array", nullable=true)
     */
    private $espaceUtilise;
    /**
     * @var float
     * @Assert\Range(
     *     min = 1,
     *     minMessage = "min 1"
     * )
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;
    /**
     * @var integer
     * @Assert\Range(
     *     min = 1,
     *     minMessage = "min 1"
     * )
     * @ORM\Column(name="nb_points", type="integer", nullable=false)
     */
    private $nbPoints;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="note", type="text", length=65535, nullable=false)
     */
    private $note;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a photo file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $brochure;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a photo file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $brochure1;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a photo file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $brochure2;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a photo file.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $brochure3;
    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="idProp",referencedColumnName="id",onDelete="CASCADE")
     */
    private $proprietaire;
    /**
     *
     * @ORM\column(name="dateDebut",type="date",length=255)
     *
     */
    private $date;
    /**
     * @Assert\Range(
     *     min = "now"
     * )
     * @ORM\column(name="datefin",type="date",length=255)
     *
     */
    private $dateFin;
    /**
     * @var string
     *
     * @ORM\Column(name="region", type="text", length=255, nullable=false)
     */
    private $region;
    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="text", length=255, nullable=false)
     */
    private $pays;


    function __construct()
    {

        $this->date = new \DateTimeImmutable();
        $this->dateFin = new \Datetime();


    }

    /**
     * @return mixed
     */
    public function getBrochure1()
    {
        return $this->brochure1;
    }

    /**
     * @param mixed $brochure1
     */
    public function setBrochure1($brochure1)
    {
        $this->brochure1 = $brochure1;
    }

    /**
     * @return mixed
     */
    public function getBrochure2()
    {
        return $this->brochure2;
    }

    /**
     * @param mixed $brochure2
     */
    public function setBrochure2($brochure2)
    {
        $this->brochure2 = $brochure2;
    }

    /**
     * @return mixed
     */
    public function getBrochure3()
    {
        return $this->brochure3;
    }

    /**
     * @param mixed $brochure3
     */
    public function setBrochure3($brochure3)
    {
        $this->brochure3 = $brochure3;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return int
     */
    public function getIdLog()
    {
        return $this->idLog;
    }

    /**
     * @param int $idLog
     */
    public function setIdLog($idLog)
    {
        $this->idLog = $idLog;
    }

    /**
     * @return string
     */
    public function getTypeChambre()
    {
        return $this->typeChambre;
    }

    /**
     * @param string $typeChambre
     */
    public function setTypeChambre($typeChambre)
    {
        $this->typeChambre = $typeChambre;
    }

    /**
     * @return int
     */
    public function getNbVoyageur()
    {
        return $this->nbVoyageur;
    }

    /**
     * @param int $nbVoyageur
     */
    public function setNbVoyageur($nbVoyageur)
    {
        $this->nbVoyageur = $nbVoyageur;
    }

    /**
     * @return string
     */
    public function getTypeHabitation()
    {
        return $this->typeHabitation;
    }

    /**
     * @param string $typeHabitation
     */
    public function setTypeHabitation($typeHabitation)
    {
        $this->typeHabitation = $typeHabitation;
    }

    /**
     * @return int
     */
    public function getNbLit()
    {
        return $this->nbLit;
    }

    /**
     * @param int $nbLit
     */
    public function setNbLit($nbLit)
    {
        $this->nbLit = $nbLit;
    }

    /**
     * @return string
     */
    public function getTypeLit()
    {
        return $this->typeLit;
    }

    /**
     * @param string $typeLit
     */
    public function setTypeLit($typeLit)
    {
        $this->typeLit = $typeLit;
    }

    /**
     * @return int
     */
    public function getNbSallebain()
    {
        return $this->nbSallebain;
    }

    /**
     * @param int $nbSallebain
     */
    public function setNbSallebain($nbSallebain)
    {
        $this->nbSallebain = $nbSallebain;
    }

    /**
     * @return string
     */
    public function getTypeSb()
    {
        return $this->typeSb;
    }

    /**
     * @param string $typeSb
     */
    public function setTypeSb($typeSb)
    {
        $this->typeSb = $typeSb;
    }

    /**
     * @return string
     */
    public function getAdressgmap()
    {
        return $this->adressgmap;
    }

    /**
     * @param string $adressgmap
     */
    public function setAdressgmap($adressgmap)
    {
        $this->adressgmap = $adressgmap;
    }

    /**
     * @return string
     */
    public function getLatlng()
    {
        return $this->latlng;
    }

    /**
     * @param string $latlng
     */
    public function setLatlng($latlng)
    {
        $this->latlng = $latlng;
    }

    /**
     * @return string
     */
    public function getEquipement()
    {
        return $this->equipement;
    }

    /**
     * @param string $equipement
     */
    public function setEquipement($equipement)
    {
        $this->equipement = $equipement;
    }

    /**
     * @return string
     */
    public function getEspaceUtilise()
    {
        return $this->espaceUtilise;
    }

    /**
     * @param string $espaceUtilise
     */
    public function setEspaceUtilise($espaceUtilise)
    {
        $this->espaceUtilise = $espaceUtilise;
    }

    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return int
     */
    public function getNbPoints()
    {
        return $this->nbPoints;
    }

    /**
     * @param int $nbPoints
     */
    public function setNbPoints($nbPoints)
    {
        $this->nbPoints = $nbPoints;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getBrochure()
    {
        return $this->brochure;
    }

    /**
     * @param mixed $brochure
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;
    }

    /**
     * @return mixed
     */
    public function getProprietaire()
    {
        return $this->proprietaire;
    }

    /**
     * @param mixed $proprietaire
     */
    public function setProprietaire($proprietaire)
    {
        $this->proprietaire = $proprietaire;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * @param mixed $dateFin
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }
}
