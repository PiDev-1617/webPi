<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Time;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * EspaceAprox
 *
 * @ORM\Table(name="espace_aprox")
 * @ORM\Entity(repositoryClass="pi\EspaceBundle\Repository\espaceRepository")
 * @Vich\Uploadable
 */
class EspaceAprox
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_espace", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idResto;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_Resto", type="string", length=255, nullable=false)
     */
    private $NomResto;

    /**
     * @var string
     *
     * @ORM\Column(name="Spec_Resto", type="array", length=255, nullable=true)
     */
    private $SpecResto;

    /**
     * @var string
     *
     * @ORM\Column(name="Serv_Resto", type="array", length=255, nullable=true)
     */
    private $ServResto;
    /**
     * @var string
     *
     * @ORM\Column(name="Offre", type="array", length=255, nullable=true)
     */
    private $Offre;

    /**
     * @var string
     *
     * @ORM\Column(name="Addresse", type="string", length=255, nullable=true)
     */
    private $addresse;

    /**
     * @var string
     *
     * @ORM\Column(name="Desc_Resto", type="text", length=535, nullable=true)
     */
    private $descResto;

    /**
     * @var string
     *
     * @ORM\Column(name="num_tel", type="string", length=255, nullable=true)
     */
    private $numTel;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=255, nullable=true)
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var time
     *
     * @ORM\Column(name="ouverture", type="time", length=10, nullable=true)
     * @Type("DateTime<'h-m-s'>")
     */
    private $timeOuv;

    /**
     * @var time
     * @Assert\Expression(
     *     "this.getTimeFer() > this.getTimeOuv()",
     *     message="La date fin de l'evenement  doit etre supérieure à la date début"
     * )
     *
     * @ORM\Column(name="fermeture", type="time", length=10, nullable=true)
     * @Type("DateTime<'h-m-s'>")
     */

    private $timeFer;



    /**
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="id_propriet",referencedColumnName="id",onDelete="CASCADE")
     */
    private $propriet;

    /**
     * @var integer
     *
     * @ORM\Column(name="solde", type="integer", length=255, nullable=true)
     */
    private $solde;
    /**
     * @var string
     *
     * @ORM\Column(name="sponsored", type="string", length=255, nullable=true)
     */
    private $sponsored;

    /*---------------image1-------------------*/

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="member_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /*---------------image1-------------------*/

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="member_image", fileNameProperty="imageName1")
     *
     * @var File
     */
    private $imageFile1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName1;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt1;

    /*---------------image2-------------------*/

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="member_image", fileNameProperty="imageName2")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName2;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt2;

    /*---------------image3-------------------*/

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="member_image", fileNameProperty="imageName3")
     *
     * @var File
     */
    private $imageFile3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName3;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt3;

    /*------------------------------------------------*/

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return EspaceAprox
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param string $imageName
     *
     * @return EspaceAprox
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /*---------------------------------------------------*/

    /**
     * @return File|null
     */
    public function getImageFile1()
    {
        return $this->imageFile1;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image1
     *
     * @return EspaceAprox
     */
    public function setImageFile1(File $image1 = null)
    {
        $this->imageFile1 = $image1;

        if ($image1) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt1 = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName1()
    {
        return $this->imageName1;
    }

    /**
     * @param string $imageName1
     *
     * @return EspaceAprox
     */
    public function setImageName1($imageName1)
    {
        $this->imageName1 = $imageName1;

        return $this;
    }

    /*-------------------------------------------------*/

    /**
     * @return File|null
     */
    public function getImageFile2()
    {
        return $this->imageFile2;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image2
     *
     * @return EspaceAprox
     */
    public function setImageFile2(File $image2 = null)
    {
        $this->imageFile2 = $image2;

        if ($image2) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt2 = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName2()
    {
        return $this->imageName2;
    }

    /**
     * @param string $imageName2
     *
     * @return EspaceAprox
     */
    public function setImageName2($imageName2)
    {
        $this->imageName2 = $imageName2;

        return $this;
    }

    /*----------------------------------------------------*/

    /**
     * @return File|null
     */
    public function getImageFile3()
    {
        return $this->imageFile3;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return EspaceAprox
     */
    public function setImageFile3(File $image3 = null)
    {
        $this->imageFile3 = $image3;

        if ($image3) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt3 = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName3()
    {
        return $this->imageName3;
    }

    /**
     * @param string $imageName3
     *
     * @return EspaceAprox
     */
    public function setImageName3($imageName3)
    {
        $this->imageName3 = $imageName3;

        return $this;
    }

    /*---------------------------------------------------*/

    /**
     * @return int
     */
    public function getIdResto()
    {
        return $this->idResto;
    }

    /**
     * @param int $idResto
     */
    public function setIdResto($idResto)
    {
        $this->idResto = $idResto;
    }

    /**
     * @return string
     */
    public function getNomResto()
    {
        return $this->NomResto;
    }

    /**
     * @param string $NomResto
     */
    public function setNomResto($NomResto)
    {
        $this->NomResto = $NomResto;
    }

    /**
     * @return string
     */
    public function getSpecResto()
    {
        return $this->SpecResto;
    }

    /**
     * @param string $SpecResto
     */
    public function setSpecResto($SpecResto)
    {
        $this->SpecResto = $SpecResto;
    }

    /**
     * @return string
     */
    public function getServResto()
    {
        return $this->ServResto;
    }

    /**
     * @param string $ServResto
     */
    public function setServResto($ServResto)
    {
        $this->ServResto = $ServResto;
    }

    /**
     * @return string
     */
    public function getOffre()
    {
        return $this->Offre;
    }

    /**
     * @param string $Offre
     */
    public function setOffre($Offre)
    {
        $this->Offre = $Offre;
    }

    /**
     * @return string
     */
    public function getAddresse()
    {
        return $this->addresse;
    }

    /**
     * @param string $addresse
     */
    public function setAddresse($addresse)
    {
        $this->addresse = $addresse;
    }

    /**
     * @return string
     */
    public function getDescResto()
    {
        return $this->descResto;
    }

    /**
     * @param string $descResto
     */
    public function setDescResto($descResto)
    {
        $this->descResto = $descResto;
    }

    /**
     * @return string
     */
    public function getNumTel()
    {
        return $this->numTel;
    }

    /**
     * @param string $numTel
     */
    public function setNumTel($numTel)
    {
        $this->numTel = $numTel;
    }

    /**
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * @param string $siteWeb
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return Time
     */
    public function getTimeOuv()
    {
        return $this->timeOuv;
    }

    /**
     * @param Time $timeOuv
     */
    public function setTimeOuv($timeOuv)
    {
        $this->timeOuv = $timeOuv;
    }

    /**
     * @return Time
     */
    public function getTimeFer()
    {
        return $this->timeFer;
    }

    /**
     * @param Time $timeFer
     */
    public function setTimeFer($timeFer)
    {
        $this->timeFer = $timeFer;
    }


    /**
     * @return mixed
     */
    public function getPropriet()
    {
        return $this->propriet;
    }

    /**
     * @param mixed $propriet
     */
    public function setPropriet($propriet)
    {
        $this->propriet = $propriet;
    }

    /**
     * @return int
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * @param int $solde
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;
    }

    /**
     * @return string
     */
    public function getSponsored()
    {
        return $this->sponsored;
    }

    /**
     * @param string $sponsored
     */
    public function setSponsored($sponsored)
    {
        $this->sponsored = $sponsored;
    }




}

