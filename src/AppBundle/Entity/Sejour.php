<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 */
class Sejour
{
    /**
     * @ORM\GeneratedValue
     * @ORM\Id
     * @ORM\Column(type = "integer")
     */
    private $idSej;
    /**
     * @ORM\Column(type = "text",length=255)
     */
    private $messageSej;
    /**
     * @var \DateTime
     * @ORM\Column(name="datedeSejour",type = "date")
     */
    private $Date;
    /**
     * @ORM\Column(type = "string",length=255)
     */
    private $ville;
    /**
     *
     * @ORM\ManyToOne(targetEntity="pi\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    /**
     * @ORM\Column(type = "string",length=255)
     */
    private $titre;
    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Please, upload your experience picture.")
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $picture;

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getIdSej()
    {
        return $this->idSej;
    }

    /**
     * @param mixed $idSej
     */
    public function setIdSej($idSej)
    {
        $this->idSej = $idSej;
    }

    /**
     * @return mixed
     */
    public function getMessageSej()
    {
        return $this->messageSej;
    }

    /**
     * @param mixed $messageSej
     */
    public function setMessageSej($messageSej)
    {
        $this->messageSej = $messageSej;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->Date;
    }

    /**
     * @param \DateTime $Date
     */
    public function setDate($Date)
    {
        $this->Date = $Date;
    }

    /**
     * @return \DateTime
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param \DateTime $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }


}