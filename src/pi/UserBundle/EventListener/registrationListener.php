<?php
/**
 * Created by PhpStorm.
 * User: Mimoo
 * Date: 2/5/2017
 * Time: 8:27 PM
 */

namespace pi\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class registrationListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS =>
            'onRegistrationSuccess'

        );
    }

    public function onRegistrationSuccess(FormEvent $event){
        $roles = array('ROLE_USER');
        $user = $event->getForm()->getData();
        $user->setRoles($roles);
    }

}