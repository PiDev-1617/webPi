<?php

namespace pi\UserBundle;

use FOS\UserBundle\FOSUserBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class piUserBundle extends Bundle
{
    public function getParent(){
        return 'FOSUserBundle';
    }
}
