<?php

namespace pi\GestionReclamationBundle\Controller;

//use FOS\UserBundle\Model\User ;
use pi\GestionReclamationBundle\Entity\Mail;
use pi\GestionReclamationBundle\Entity\MailType;
use pi\GestionReclamationBundle\Entity\Reclamation;
use pi\GestionReclamationBundle\Entity\ReclamationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piGestionReclamationBundle:Default:index.html.twig');
    }

    public function calenderAction()
    {
        return $this->render('piGestionReclamationBundle:Default:calender.html.twig');
    }

    public function mailboxAction()
    {
        return $this->render('piGestionReclamationBundle:Default:mailbox.html.twig');
    }

    public function composeAction()
    {
        return $this->render('piGestionReclamationBundle:Default:compose.html.twig');
    }

    public function readmailAction()
    {
        return $this->render('piGestionReclamationBundle:Default:read.html.twig');
    }

    public function indexmailAction(Request $request)

    {

        $mail = new Mail();

        $form = $this->createForm(MailType::class, $mail);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $message = \Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('host.gest123@gmail.com')
                ->setTo($mail->getEmail())
                ->setBody(

                    $this->renderView(

                        'piGestionReclamationBundle:Default:email.html.twig',

                        array('nom' => $mail->getNom(), 'prenom' => $mail->getPrenom())

                    ),

                    'text/html'

                );

            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('test_send_mail_success'));
        }

        return $this->render('piGestionReclamationBundle:Default:index.html.twig', array('form' => $form
            ->createView()));
    }

    public function successAction()
    {

        return new Response("email envoyé avec succès, Merci de vérifier votre adresse mail

.");

    }

    public function layoutAction()
    {
        return $this->render('piGestionReclamationBundle:Default:layout.html.twig');

    }

    public function userpageAction()
    {
        return $this->render('user.html.twig');
    }

    public function ajoutReclamationAction(Request $request)
    {
        $reclamation = new Reclamation();
        $form = $this->createForm(ReclamationType::class, $reclamation);
        if ($form->handleRequest($request)->isValid()) {
            $orm = $this->getDoctrine()->getManager();
            $container = new ContainerBuilder();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $userId = $user->getId();
            $reclamation->setIdUser($userId);
            $reclamation->setDate();
            $reclamation->setLire(0);
            $orm->persist($reclamation);
            $orm->flush();
            //envoit de email avec le swiftmailer
            $message = \Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('host.gest123@gmail.com')
                ->setTo($reclamation->getFrom())
                ->setBody($reclamation->getMessage());

            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('test_send_mail_success'));

        }
        return $this->render("piGestionReclamationBundle:Default:reclamation.html.twig", array('form' => $form->createView()));
    }

    public function affichageAction()
    {
        $orm = $this->getDoctrine()->getManager(); //instanciation de ORM
        $reclamation = $orm->getRepository("piGestionReclamationBundle:Reclamation")->findAll();
        return $this->render("piGestionReclamationBundle:Default:data.html.twig", array('reclamation' => $reclamation));
    }

    public function rechercheAction(Request $request)
    {
        $rec = new Reclamation();


        if ($request->isMethod('POST')) {
            $date_rec = $request->get('date_rec');
            $orm = $this->getDoctrine()->getManager();
            $reclamation = $orm->getRepository("piGestionReclamationBundle:Reclamation")->findQbDate($date_rec);
            return $this->render("piGestionReclamationBundle:Default:data.html.twig", array('reclamation' => $reclamation));

        }
        return $this->render("piGestionReclamationBundle:Default:recherche2.html.twig", array('modele' => $rec));
    }

    public function affichageRecUserAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $userId = $user->getId();
        $orm = $this->getDoctrine()->getManager();
        $reclamation = $orm->getRepository("piGestionReclamationBundle:Reclamation")->findQbId($userId);
        return $this->render("piGestionReclamationBundle:Default:reclamationUser.html.twig", array('reclamation' => $reclamation));


    }

    public function modifierAction(Request $request, $id_Rec)
    {

        $orm = $this->getDoctrine()->getManager();
        $rec = $orm->getRepository("piGestionReclamationBundle:Reclamation")->find($id_Rec);
        $form = $this->createForm(ReclamationType::class, $rec);

        if ($form->handleRequest($request)->isValid()) {
            $orm = $this->getDoctrine()->getManager();
            $orm->persist($rec);
            $orm->flush();
            return $this->redirectToRoute("user_reclamation");
        }
        return $this->render("piGestionReclamationBundle:Default:update.html.twig", array('form' => $form->createView()));

    }

    public function accepterAction($id_Rec)
    {
        $orm = $this->getDoctrine()->getManager();
        $rec = $orm->getRepository("piGestionReclamationBundle:Reclamation")->find($id_Rec);
        $user = $orm->getRepository("piUserBundle:User")->find($rec->getIdUser());
        $rec->setLire(1);
        $orm = $this->getDoctrine()->getManager();
        $orm->persist($rec);
        $orm->flush();

        $message = \Swift_Message::newInstance()
            ->setSubject($rec->getSubject())
            ->setFrom('host.gest123@gmail.com')
            ->setTo($rec->getFrom())
            ->setBody('Merci Mr/Mm ' . $user->getUsername() . ' votre réclamation a été prise en compte !');

        $this->get('mailer')->send($message);

        return $this->redirect($this->generateUrl('test_send_mail_success'));


    }

    public function refuserAction($id_Rec)
    {
        $orm = $this->getDoctrine()->getManager();
        $rec = $orm->getRepository("piGestionReclamationBundle:Reclamation")->find($id_Rec);
        $user = $orm->getRepository("piUserBundle:User")->find($rec->getIdUser());

        $message = \Swift_Message::newInstance()
            ->setSubject($rec->getSubject())
            ->setFrom('host.gest123@gmail.com')
            ->setTo($rec->getFrom())
            ->setBody('Merci Mr/Mm ' . $user->getUsername() . ' votre réclamation envoyée le a été réfusé manque de justification !');

        $this->get('mailer')->send($message);
        $orm->remove($rec);
        $orm->flush();


        return $this->redirect($this->generateUrl('test_send_mail_success'));
    }


    public function querybAction()
    {
        $orm = $this->getDoctrine()->getManager();
        $rec = $orm->getRepository("piGestionReclamationBundle:Reclamation")->findQb();
        return new Response(count($rec));
    }

}
