<?php
/**
 * Created by PhpStorm.
 * User: EL PANDAs
 * Date: 17/02/2017
 * Time: 21:32
 */

namespace pi\GestionReclamationBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */

/**
 * @ORM\Entity(repositoryClass="pi\GestionReclamationBundle\Entity\ReclamationRepository")
 */
class Reclamation
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_Reclamation;
    /**
     * @ORM\Column(type="string",length=255)
     */
    private $emetteur;
    /*
        /
         * @param mixed $id_Reclamation
         */
    /*public function setIdReclamation($id_Reclamation)
    {
        $this->id_Reclamation = $id_Reclamation;
    }*/
    /**
     * @ORM\Column(type="string",length=255)
     */
    private $subject;
    /**
     * @ORM\Column(type="string",length=255)
     */
    private $message;
    /**
     * @ORM\Column(type="integer")
     */
    private $id_user;
    /**
     * @ORM\Column(type="date")
     */
    private $dateRec;
    /**
     * @ORM\Column(type="integer")
     */
    private $lire;

    /**
     * @return mixed
     */
    public function getIdReclamation()
    {
        return $this->id_Reclamation;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->emetteur;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->emetteur = $from;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * @param mixed $id_user
     */
    public function setIdUser($id_user)
    {
        $this->id_user = $id_user;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->dateRec;
    }

    /**
     * @param mixed $date
     */
    public function setDate()
    {
        $this->dateRec = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getLire()
    {
        return $this->lire;
    }

    /**
     * @param mixed $lire
     */
    public function setLire($lire)
    {
        $this->lire = $lire;
    }


}