<?php
/**
 * Created by PhpStorm.
 * User: EL PANDAs
 * Date: 18/02/2017
 * Time: 22:29
 */

namespace pi\GestionReclamationBundle\Entity;


use Doctrine\ORM\EntityRepository;

class ReclamationRepository extends EntityRepository
{
    public function findQb()
    {
        $qb = $this->getEntityManager()
            ->createQuery("SELECT m from piGestionReclamationBundle:Reclamation m WHERE m.lire=0");

        return $qb->getResult();
    }

    public function findQbDate($date1)
    {
        $qb = $this->getEntityManager()
            ->createQuery("SELECT m from piGestionReclamationBundle:Reclamation m WHERE m.dateRec=:param")
            ->setParameter('param', $date1);   // initialisé le param dans notre query

        return $qb->getResult();
    }


    public function findQbId($Userid)
    {
        $qb = $this->getEntityManager()
            ->createQuery("SELECT m from piGestionReclamationBundle:Reclamation m WHERE m.id_user=:param")
            ->setParameter('param', $Userid);   // initialisé le param dans notre query

        return $qb->getResult();
    }

}