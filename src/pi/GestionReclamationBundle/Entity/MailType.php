<?php
/**
 * Created by PhpStorm.
 * User: EL PANDAs
 * Date: 12/02/2017
 * Time: 16:44
 */

namespace pi\GestionReclamationBundle\Entity;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class MailType extends AbstractType

{

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('tel', IntegerType::class)
            ->add('email', EmailType::class)
            ->add('text', TextareaType::class)
            ->add('valider', SubmitType::class);

    }

    public function getName()

    {

        return 'Mail';

    }

}



