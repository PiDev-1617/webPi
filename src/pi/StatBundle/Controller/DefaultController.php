<?php

namespace pi\StatBundle\Controller;

use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piStatBundle:Default:index.html.twig');
    }

    public function chartPieAction()
    {
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('Top villes visitées');
        $ob->plotOptions->pie(array('allowPointSelect' => true, 'cursor' => 'pointer', 'dataLabels' => array('enabled' => false), 'showInLegend' => true));
        $em = $this->getDoctrine()->getEntityManager();
        $Evenement = $em->getRepository('AppBundle:Evenement')->findAll();
        $totalplace = 0;
        foreach ($Evenement as $event) {
            $totalplace = $totalplace + $event->getTicketsDispo();
        }
        $data = array();
        foreach ($Evenement as $evenement) {
            $stat = array();
            array_push($stat, $evenement->getNomEvent(), (($evenement->getTicketsDispo()) * 100) / $totalplace);
            //var_dump($stat); array_push($data,$stat);
        }
        // var_dump($data);
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('piStatBundle::top_villes.html.twig', array('chart' => $ob));
    }
}
