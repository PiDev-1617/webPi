<?php

namespace pi\SejourBundle\Controller;

use AppBundle\Entity\Sejour;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Sejour controller.
 *
 */
class SejourController extends Controller
{
    /**
     * Lists all sejour entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sejours = $em->getRepository('AppBundle:Sejour')->findAll();

        return $this->render('@piSejour/sejour/index.html.twig', array(
            'sejours' => $sejours,
        ));
    }

    /**
     * Creates a new sejour entity.
     *
     */
    public function newAction(Request $request)
    {
        $sejour = new Sejour();
        $form = $this->createForm('pi\SejourBundle\Form\SejourType', $sejour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $sejour->getPicture();

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('brochures_directory'),
                $fileName
            );


            $sejour->setPicture($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($sejour);
            $em->flush($sejour);

            return $this->redirectToRoute('sejour_show', array('id' => $sejour->getIdSej()));
        }

        return $this->render('@piSejour/sejour/new.html.twig', array(
            'sejour' => $sejour,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{_locale}", defaults={"_locale": "en"}, requirements={
     *     "_locale": "en|fr"
     * })
     */
    public function showAction(Sejour $sejour)
    {
        $deleteForm = $this->createDeleteForm($sejour);

        return $this->render('@piSejour/sejour/show.html.twig', array(
            'sejour' => $sejour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a sejour entity.
     *
     * @param Sejour $sejour The sejour entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sejour $sejou)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sejour_delete', array('idSej' => $sejou->getIdsej())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing sejour entity.
     *
     */
    public function editAction(Request $request, Sejour $sejour)
    {
        $deleteForm = $this->createDeleteForm($sejour);
        $editForm = $this->createForm('pi\SejourBundle\Form\SejourType', $sejour);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sejour_edit', array('idSej' => $sejour->getIdsej()));
        }

        return $this->render('@piSejour/sejour/edit.html.twig', array(
            'sejour' => $sejour,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sejour entity.
     *
     */
    public function deleteAction(Request $request, Sejour $sejou)
    {
        $form = $this->createDeleteForm($sejou);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sejou);
            $em->flush();
        }

        return $this->redirectToRoute('sejour_index');
    }
}
