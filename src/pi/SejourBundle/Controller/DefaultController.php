<?php

namespace pi\SejourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piSejourBundle:Default:index.html.twig');
    }
}
