<?php

namespace pi\EventBundle\Form;

;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvenementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('NomEvent')
            ->add('CategorieEvent', ChoiceType::class, array('choices' =>
                array('Festival de musique' => 'Festival de musique',
                    'Concert musical' => 'Concert musical',
                    'Compétition Sportive' => 'Compétition sportive',
                    'Evenement Culturel' => 'Evenement Culturel',
                    'Evenement politique' => 'Evenement politique',
                    'Evenement de sensibilisation' => 'Evenement de sensibilisation',
                    'Autre' => 'Autre')))
            ->add('ville')
            ->add('dateDebut', DateType::class, array(
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
                )
            ))
            ->add('dateFin', DateType::class, array(
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day'
                )
            ))
            ->add('Horaire')
            ->add('description', TextType::class)
            ->add('TicketsDispo')
            ->add('prixPass')
            ->add('lienFb', TextType::class)
            ->add('Email', EmailType::class)
            ->add('NumTel', TextType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Evenement'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pi_Eventbundle_evenement';
    }


}
