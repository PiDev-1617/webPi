<?php


namespace pi\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;


class EvenementRepository extends EntityRepository
{


    public function findEventByAddrs($lieu)
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.ville like :ville')
            ->setParameter('ville', $lieu . '%')
            ->orderBy('e.ville', 'ASC')
            ->getQuery();
        return $query->getResult();
    }

    public function findAvailableEvents()
    {
        $query = $this->getEntityManager()->createQuery("SELECT ev FROM AppBundle:Evenement ev WHERE ev.TicketsDispo > 0 ");
        return $query->getResult();
    }

    public function findProchEvent()
    {

        $query = $this->getEntityManager()->createQuery("SELECT ev FROM AppBundle:Evenement ev WHERE ev.dateDebut BETWEEN CURRENT_DATE() AND CURRENT_DATE()+10");
        return $query->getResult();
    }

    public function findNearEvent()
    {

        $query = $this->getEntityManager()->createQuery("SELECT ev FROM AppBundle:Evenement ev WHERE ev.ville like 'Tunis'");
        return $query->getResult();
    }

    public function removeEventPart($idev)
    {
        $query = $this->getEntityManager()->createQuery("delete  AppBundle:EventParticipation p where p.evenement=:id")
            ->setParameter('id', $idev);
        return $query->execute();
    }


}