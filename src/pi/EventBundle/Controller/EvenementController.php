<?php

namespace pi\EventBundle\Controller;

use AppBundle\Entity\Evenement;
use AppBundle\Entity\EventParticipation;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EvenementController extends Controller
{
    /**
     * Lists all evenement entities.
     *
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $criteres = $request->get('criteria');
        $participation = new EventParticipation();
        $em = $this->getDoctrine()->getManager();
        switch ($criteres) {
            case 'mes_évènements':
                $evenements = $em->getRepository('AppBundle:Evenement')->findBy(array('Organisateur' => $this->getUser()));
                break;
            case 'disponibilté':
                $evenements = $em->getRepository('AppBundle:Evenement')->findAvailableEvents();
                break;

            case 'Date':
                $evenements = $em->getRepository('AppBundle:Evenement')->findBy(array(), array('dateDebut' => 'ASC'));
                break;
            default:
                $evenements = $em->getRepository('AppBundle:Evenement')->findProchEvent();
                break;
        }

        $paginator = $this->get('knp_paginator');
        $evenements = $paginator->paginate($evenements, $request->query->getInt('page', 1),
            $request->query->getInt('limit', 4));

        return $this->render('piEventBundle:evenement:index.html.twig', array(
            'evenements' => $evenements, 'criteria' => $criteres, 'participation' => $participation
        ));
    }

    /**
     * Creates a new evenement entity.
     *
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $evenement = new Evenement();
        $user = $this->getUser();
        $form = $this->createForm('pi\EventBundle\Form\EvenementType', $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (($evenement->getTicketsDispo() < 0) or ($evenement->getDateFin() < $evenement->getDateDebut()) or ($evenement->getPrixPass() < 0)) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Veuillez saisir un nombre de Tickets positive ou une date de fin valide !'
                );
                return $this->redirectToRoute('evenement_new');
            }
            $em = $this->getDoctrine()->getManager();
            $evenement->setOrganisateur($user);

            $em->persist($evenement);
            $em->flush();

            return $this->redirectToRoute('evenement_show', array('id' => $evenement->getIdEv()));
        }

        return $this->render('piEventBundle:evenement:new.html.twig', array(
            'evenement' => $evenement,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a evenement entity.
     *
     */
    public function showAction(Evenement $evenement)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $deleteForm = $this->createDeleteForm($evenement);

        return $this->render('piEventBundle:evenement:show.html.twig', array(
            'evenement' => $evenement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a evenement entity.
     *
     * @param Evenement $evenement The evenement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Evenement $evenement)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evenement_delete', array('id' => $evenement->getIdEv())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing evenement entity.
     *
     */
    public function editAction(Request $request, Evenement $evenement)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $deleteForm = $this->createDeleteForm($evenement);
        $editForm = $this->createForm('pi\EventBundle\Form\EvenementType', $evenement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('evenement_index', array('id' => $evenement->getIdEv()));
        }

        return $this->render('piEventBundle:evenement:edit.html.twig', array(
            'evenement' => $evenement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a evenement entity.
     *
     */
    public function deleteAction(Request $request, Evenement $evenement)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createDeleteForm($evenement);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->getRepository('AppBundle:Evenement')->removeEventPart($evenement->getIdEv());
            $em->remove($evenement);
            $em->flush();
        }

        return $this->redirectToRoute('evenement_index');
    }

    public function ParticiperAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $participation = new EventParticipation();


        $user = $this->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $ev = $request->get('id');

        $idEv = $em->getRepository('AppBundle:Evenement')->find($ev);
        $participation->setEvenement($idEv);


        $participation->setParticipant($user);
        $participation->setStatus(0);
        $em->persist($participation);
        $em->flush();
        $idEv->setTicketsDispo($idEv->getTicketsDispo() - 1);
        $em->persist($idEv);
        $em->flush();


        return $this->render('piEventBundle:evenement:participation.html.twig');

    }

    public function chartPieAction()
    {
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('Top villes visitées');
        $ob->plotOptions->pie(array('allowPointSelect' => true,
            'cursor' => 'pointer', 'dataLabels' => array('enabled' => false),
            'showInLegend' => true));
        $em = $this->getDoctrine()->getEntityManager();
        $Evenement = $em->getRepository('AppBundle:Evenement')->findAll();
        $totalplace = 0;
        foreach ($Evenement as $event) {
            $totalplace = $totalplace + $event->getTicketsDispo();
        }
        $data = array();
        foreach ($Evenement as $evenement) {
            $stat = array();
            array_push($stat, $evenement->getNomEvent(), (($evenement->getTicketsDispo()) * 100) / $totalplace);
            //var_dump($stat); array_push($data,$stat);
        }
        // var_dump($data);
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('piEventBundle:evenement:top_villes.html.twig', array('chart' => $ob));
    }

}
