<?php

namespace pi\EventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piEventBundle:Default:index.html.twig');
    }
}
