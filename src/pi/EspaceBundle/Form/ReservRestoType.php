<?php

namespace pi\EspaceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservRestoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('Requirement')
            ->add('Debutrez')
            ->add('Finrez')
            ->add('NbrePers')
            ->add('nbrrp');


        /*   ->add('remiserez',EntityType::class ,
           array('class'=>'AppBundle\Entity\EspaceAprox' ,'choice_label'=>'offre'))
               ->add('resto',EntityType::class ,
                   array('class'=>'AppBundle\Entity\EspaceAprox' ,'choice_label'=>'idResto'))
           ->add('Client',EntityType::class ,
           array('class'=>'pi\UserBundle\Entity\User' ,'choice_label'=>'id'));*/
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ReservResto'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_reservresto';
    }


}
