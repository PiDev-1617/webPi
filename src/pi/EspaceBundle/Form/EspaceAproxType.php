<?php

namespace pi\EspaceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EspaceAproxType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('NomResto')
            ->add('SpecResto', ChoiceType::class, array('choices' => array(
                'Cuisine de Quartier' => "Cuisine de Quartier",
                'Cuisine Francaise' => "Cuisine Francaise",
                'Cuisine Italienne' => "Cuisine Italienne",
                'Cuisine Gastronomique' => "Cuisine Gastronomique",
                'Cuisine Orientale' => "Cuisine Orientale",
                'Cuisine Japonaise' => "Cuisine Japonaise",
                'Cuisine Thailandaise' => "Cuisine Thailandaise",
                'autres' => "Autres"),
                'multiple' => true, 'expanded' => true
            ))
            ->add('ServResto', ChoiceType::class, array('choices' => array(
                'Avec places assises' => "Avec places assises",
                'Plats à emporter' => "Plats à emporter",
                'Fast-food' => "Fast-food",
                'Sur réservation uniquement' => "Sur réservation uniquement",
                'Dîner-spectacle' => "Dîner-spectacle",
                'Autres Services' => "Autres Services"),
                'multiple' => true, 'expanded' => true
            ))
            ->add('Offre', ChoiceType::class, array('choices' => array(
                'Remise de 20%' => "Remise de 20%",
                'Un Repas Gratuit' => "Un Repas Gratuit",
                'Un Boisson Gratuit' => "Un Boisson Gratuit"),
                'multiple' => true, 'expanded' => true
            ))
            ->add('addresse')
            ->add('descResto')
            ->add('numTel')
            ->add('facebook')
            ->add('siteWeb')
            ->add('email')
            ->add('timeOuv')
            ->add('timeFer')
            ->add('imageFile', FileType::class, array(
                "required" => FALSE,
                'data_class' => null,
            ))
            ->add('imageFile1', FileType::class, array(
                "required" => FALSE,
                'data_class' => null,
            ))
            ->add('imageFile2', FileType::class, array(
                "required" => FALSE,
                'data_class' => null,
            ))
            ->add('imageFile3', FileType::class, array(
                "required" => FALSE,
                'data_class' => null,
            ))
            ->add('sponsored', ChoiceType::class, array('choices' => array(
                'parrainer' => "parrainer", 'non parrainer' => null,),
                'multiple' => false, 'expanded' => true,
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\EspaceAprox'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pi_espacebundle_espaceaprox';
    }


}
