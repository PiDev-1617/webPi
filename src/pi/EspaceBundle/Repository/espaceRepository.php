<?php
namespace pi\EspaceBundle\Repository;

/**
 * Created by PhpStorm.
 * User: Kossayy
 * Date: 13/02/2017
 * Time: 01:13
 */

class espaceRepository extends \Doctrine\ORM\EntityRepository
{
    /*    public function findLogementByDestianation($add)
        {
            $query = $this->createQueryBuilder('x')
                ->where('x.addresse like :addresse')
                ->setParameter('addresse', $add . '%')
                ->orderBy('x.addresse', 'ASC')
                ->getQuery();
            return $query->getResult();
        }*/


    public function annulrez()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT p FROM AppBundle:ReservResto p WHERE p.Rstatut = 0 ORDER BY p.Debutrez ')
            ->setMaxResults(1)->getResult();
    }

    public function ajoutstat()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT COUNT (p) FROM AppBundle:ReservResto p WHERE p.Rstatut=:k')->setParameter('k', '1')
            ->getScalarResult();

    }

    public function remise()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT  (p) FROM AppBundle: p WHERE p.Rstatut=:k')->setParameter('k', '1')
            ->getScalarResult();
    }


    public function findrestobyID($ville)
    {
        $query = $this->getEntityManager()->createQuery("SELECT ev FROM AppBundle:EspaceAprox ev WHERE ev.addresse LIKE ev.addresse = :ville");

        return $query->getResult();
    }

}