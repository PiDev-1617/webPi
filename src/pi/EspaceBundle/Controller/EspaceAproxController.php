<?php

namespace pi\EspaceBundle\Controller;

use AppBundle\Entity\EspaceAprox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Espaceaprox controller.
 *
 */
class EspaceAproxController extends Controller
{
    /**
     * Lists all espaceAprox entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $espaceAproxes = $em->getRepository('AppBundle:EspaceAprox')->findAll();

        return $this->render('piEspaceBundle:espaceaprox:index.html.twig', array(
            'espaceAproxes' => $espaceAproxes,
        ));
    }

    /**
     * Creates a new espaceAprox entity.
     *
     */
    public function newAction(Request $request)
    {
        $espaceAprox = new Espaceaprox();
        $form = $this->createForm('pi\EspaceBundle\Form\EspaceAproxType', $espaceAprox);
        $form->handleRequest($request);
        $user = $this->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $espaceAprox->setPropriet($user);
            $espaceAprox->setSolde(100);
            $spon = $espaceAprox->getSponsored();
            if ($spon != null) {
                $espaceAprox->setSolde($espaceAprox->getSolde() - 10);
            }

            $em->persist($espaceAprox);
            $em->flush();

            return $this->redirectToRoute('espace_show', array('id' => $espaceAprox->getIdResto()));
        }

        return $this->render('@piEspace/espaceaprox/new.html.twig', array(
            'espaceAprox' => $espaceAprox,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a espaceAprox entity.
     *
     */
    public function showAction(EspaceAprox $espaceAprox)
    {
        $deleteForm = $this->createDeleteForm($espaceAprox);
        $user = $this->getUser()->getId();

        return $this->render('piEspaceBundle:espaceaprox:show.html.twig', array(
            'espaceAprox' => $espaceAprox,
            'delete_form' => $deleteForm->createView(),
            'user' => $user,
        ));

    }

    /**
     * Creates a form to delete a espaceAprox entity.
     *
     * @param EspaceAprox $espaceAprox The espaceAprox entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EspaceAprox $espaceAprox)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('espace_delete', array('id' => $espaceAprox->getIdResto())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing espaceAprox entity.
     *
     */
    public function editAction(Request $request, EspaceAprox $espaceAprox)
    {
        $deleteForm = $this->createDeleteForm($espaceAprox);
        $editForm = $this->createForm('pi\EspaceBundle\Form\EspaceAproxType', $espaceAprox);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('espace_show', array('id' => $espaceAprox->getIdResto()));
        }

        return $this->render('piEspaceBundle:espaceaprox:edit.html.twig', array(
            'espaceAprox' => $espaceAprox,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a espaceAprox entity.
     *
     */
    public function deleteAction(Request $request, EspaceAprox $espaceAprox)
    {
            $em = $this->getDoctrine()->getManager();
            $em->remove($espaceAprox);
            $em->flush();


        return $this->redirectToRoute('espace_new');
    }


    public function rechercheeventAction(Request $request)
    {
        $espace = new EspaceAprox();
        $em = $this->getDoctrine()->getManager();

        $y = $request->get('rechespace');

        $espace->setAddresse($y);
        if ($y != "") {
            $espace = $em->getRepository('AppBundle:EspaceAprox')->findBy(array('addresse' => $espace->getAddresse()));
            return $this->render('piEspaceBundle:espaceaprox:recherche.html.twig', array('espace' => $espace));
        } else {
            return $this->render('piEspaceBundle:espaceaprox:recherche.html.twig', array('espace' => $espace));
        }

        return $this->render('piEspaceBundle:espaceaprox:recherche.html.twig', array('espace' => $espace));

    }


    /*
        public function findcircuitAction(Request $request)
        {
            $na = $request->get('addresse');
            $em = $this->getDoctrine()->getManager();
            if (!$na == "") {
                $pub = $em->getRepository('AppBundle:EspaceAprox')->findBy(array('addresse' => $na));

                return $this->render('CircuitPersonnelBundle:circuit personnel:find.html.twig', array( 'us' => $this->getUser(),'event' => $pub));
            }


        }*/





















}
