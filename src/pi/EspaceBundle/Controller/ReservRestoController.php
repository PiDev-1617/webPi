<?php

namespace pi\EspaceBundle\Controller;

use AppBundle\Entity\ReservResto;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Reservresto controller.
 *
 */
class ReservRestoController extends Controller
{
    /**
     * Lists all reservResto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reservRestos = $em->getRepository('AppBundle:ReservResto')->findAll();

        return $this->render('@piEspace/reservresto/index.html.twig', array(
            'reservRestos' => $reservRestos,
        ));
    }

    /**
     * Creates a new reservResto entity.
     *
     */
    public function AjoutRezAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $numres = $em->getRepository('AppBundle:ReservResto')->findBy(array('Client' => $user));
        $xy = count($numres);
        if ($xy > 0) {
            return $this->render('@piEspace/reservresto/impo.html.twig', array('rez' => $numres[0]));
        }

        $reservResto = new Reservresto();
        $form = $this->createForm('pi\EspaceBundle\Form\ReservRestoType', $reservResto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reservResto->setClient($user);
            if ($reservResto->getNbrePers() < 1 || $reservResto->getNbrePers() > 4) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Veuillez saisir un nombre de personnes approprié'
                );
                return $this->render('@piEspace/reservresto/new.html.twig', array(
                    'reservResto' => $reservResto,
                    'form' => $form->createView()));

            }
            if ($reservResto->getNbrrp() < 1 || $reservResto->getNbrrp() > 3) {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Veuillez saisir un nombre de repas approprié'
                );
                return $this->render('@piEspace/reservresto/new.html.twig', array(
                    'reservResto' => $reservResto,
                    'form' => $form->createView()));

            }
            $espace = $em->getRepository('AppBundle:EspaceAprox')->find($id);
            $x = $request->get('proprietaire');
            $var = $em->getRepository('HostGuestBundle:Reservation')->findBy(array('proprietaire' => $user));
            if ($var != null) {

                $reservResto->setRemiserez($espace->getOffre());

            }
            $reservResto->setResto($espace);
            $res = $em->getRepository('AppBundle:ReservResto')->findAll();

            $stat = $em->getRepository('AppBundle:ReservResto')->ajoutstat();
            $e = array();
            foreach ($stat as $e) {

                $x = array();
                $x = $e['1'];

                array_push($e, $x);
            }
            if ($e['1'] < 2) {
                $reservResto->setRstatut(1);
            } else {
                $reservResto->setRstatut(0);
            }

            ($reservResto->getRstatut());

            $em->persist($reservResto);
            $em->flush();

            return $this->redirectToRoute('reservresto_show', array('id' => $reservResto->getIdRez()));
        }

        return $this->render('@piEspace/reservresto/new.html.twig', array(
            'reservResto' => $reservResto,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a reservResto entity.
     *
     */
    public function showAction(ReservResto $reservResto)
    {
        $deleteForm = $this->createDeleteForm($reservResto);

        return $this->render('@piEspace/reservresto/show.html.twig', array(
            'reservResto' => $reservResto,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a reservResto entity.
     *
     * @param ReservResto $reservResto The reservResto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ReservResto $reservResto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reservresto_delete', array('id' => $reservResto->getIdRez())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Displays a form to edit an existing reservResto entity.
     *
     */
    public function editAction(Request $request, ReservResto $reservResto)
    {
        $deleteForm = $this->createDeleteForm($reservResto);
        $editForm = $this->createForm('pi\EspaceBundle\Form\ReservRestoType', $reservResto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('reservresto_edit', array('id' => $reservResto->getIdRez()));
        }

        return $this->render('@piEspace/reservresto/edit.html.twig', array(
            'reservResto' => $reservResto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reservResto entity.
     *
     */
    public function deleteAction(Request $request, ReservResto $reservResto)
    {
        $form = $this->createDeleteForm($reservResto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->remove($reservResto);
            $sta = $em->getRepository('AppBundle:ReservResto')->ajoutstat();
            $e = array();
            foreach ($sta as $e) {


                $x = $e['1'];

                array_push($e, $x);
            }
            if ($e['1'] <= 3) {
                $stat = $em->getRepository('AppBundle:ReservResto')->annulrez();
                $a = (object)$stat;
                foreach ($a as $x) {
                    $x->setRstatut(1);
                }
            }
            $em->flush();
        }

        return $this->redirectToRoute('reservresto_index');
    }
}
