<?php

namespace pi\EspaceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piEspaceBundle:Default:index.html.twig');
    }
}
