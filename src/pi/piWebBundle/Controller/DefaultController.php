<?php

namespace pi\piWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('pipiWebBundle:Default:index.html.twig');
    }

}
