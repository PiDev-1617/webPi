<?php

namespace pi\piWebBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class testController extends Controller
{
    public function indexAction()
    {
        return $this->render('@pipiWeb/Default/recherche.html.twig');
    }
    public function layoutAction()
    {
        return $this->render('@pipiWeb/layout.html.twig');
    }
    public function rechercheAction()
    {
        return $this->render('@pipiWeb/Default/recherche.html.twig');
    }
    public function detailsAction()
    {
        return $this->render('@pipiWeb/Default/details.html.twig');
    }
    public function ajoutHAction()
    {
        return $this->render('@pipiWeb/Default/ajoutHebergement.html.twig');
    }
    public function ajoutEventAction()
    {
        return $this->render('@pipiWeb/Default/ajoutEvent.html.twig');
    }
    public function mailboxAction()
    {
        return $this->render('@pipiWeb/adminDashbord/mailbox.html.twig');
    }

    public function composeAction()
    {
        return $this->render('@pipiWeb/adminDashbord/compose.html.twig');
    }

    public function readmailAction()
    {
        return $this->render('@pipiWeb/adminDashbord/read.html.twig');
    }
    public function widgetAction()
    {
        return $this->render('@pipiWeb/adminDashbord/widget.html.twig');
    }
    public function calenderAction()
    {
        return $this->render('@pipiWeb/adminDashbord/calender.html.twig');
    }

    public function affichageAction()
    {
        {
            $orm=$this->getDoctrine()->getManager(); //instanciation de ORM
            $user =$orm->getRepository("pipiWebBundle:FosUser")->findAll();
            return $this->render("@pipiWeb/Default/data.html.twig",array('user'=>$user));
        }

    }
    public function layoutadminAction()
    {
        return $this->render('@pipiWeb/adminDashbord/layoutadmin.html.twig');
    }
    public function showAction()
    {
        return $this->render('@pipiWeb/Default/admin.html.twig');
    }
    public function ReservationAction()
    {
        //return $this->render('', array('name' => $name));
        /// return $this->render('@FOSUser/Registration/register.html.twig');
        return $this->render('@pipiWeb/Default/reservation.html.twig');
    }
    public function addPropAction()
    {
        return $this->render('@pipiWeb/Default/addpropriety.html.twig');
    }
    public function NewRestoAction()
    {
        return $this->render('@pipiWeb/Default/NewResto.html.twig');
    }
    public function ajoutRestoAction()
    {
        return $this->render('@pipiWeb/Default/AjoutResto.html.twig');
    }



}
