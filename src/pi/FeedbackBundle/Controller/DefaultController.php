<?php

namespace pi\FeedbackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('piFeedbackBundle:Default:index.html.twig');
    }
}
