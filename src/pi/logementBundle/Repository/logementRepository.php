<?php
/**
 * Created by PhpStorm.
 * User: Mimoo
 * Date: 2/11/2017
 * Time: 8:04 PM
 */

namespace pi\logementBundle\Repository;
class logementRepository extends \Doctrine\ORM\EntityRepository
{
    public function findLogementByDestianation($motcle){
        $query = $this->createQueryBuilder('l')
            ->where('l.region like :region')
            ->setParameter('region', $motcle . '%')
            ->orderBy('l.date', 'Desc')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogementByDate($motcle){
        $query = $this->createQueryBuilder('l')
            ->where(' :dated between l.date and l.dateFin ')
            ->setParameter('dated', $motcle)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogementByDateDest($d,$motcle){
        $query = $this->createQueryBuilder('l')
            ->where(' :dated between l.date and l.dateFin and l.region like :region ')
            ->setParameter('dated', $d)->setParameter('region', $motcle . '%')
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogementByGuest($guest){
        $query = $this->createQueryBuilder('l')
            ->where(' l.nbVoyageur >= :voyageur ')
            ->setParameter('voyageur', $guest)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogement($d,$motcle,$guest){
        $query = $this->createQueryBuilder('l')
            ->where(' :dated between l.date and l.dateFin and l.region like :region and l.nbVoyageur > :voyageur ')
            ->setParameter('dated', $d)->setParameter('region', $motcle . '%')->setParameter('voyageur', $guest)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogementDV($d,$guest){
        $query = $this->createQueryBuilder('l')
            ->where(' :dated between l.date and l.dateFin and l.nbVoyageur > :voyageur ' )
            ->setParameter('dated', $d)->setParameter('voyageur', $guest)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }
    public function findLogementDestV($d,$guest){
        $query = $this->createQueryBuilder('l')
            ->where('l.region like :region and l.nbVoyageur > :voyageur ')
            ->setParameter('region', $d)->setParameter('voyageur', $guest)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return $query->getResult();
    }

    public function findLogementbyid($id)
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.proprietaire = :id')
            ->setParameter('id', $id)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return $query->getResult();
    }

    public function findNbLogementbyid($id)
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.proprietaire = :id')
            ->setParameter('id', $id)
            ->getQuery();
        return $query->getResult();
    }

    public function findalllogement()
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.dateFin < current_timestamp()')
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return $query->getResult();
    }

    public function findNbLogementbydate($id)
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.proprietaire = :id and l.dateFin < current_timestamp()')
            ->setParameter('id', $id)
            ->orderBy('l.date', 'DESC')
            ->getQuery();
        return  $query->getResult();
    }

    public function getlogement($id)
    {
        return $this->createQueryBuilder('a')
            ->where('a.idLog = :id')// Date de modification antérieure à :date
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}