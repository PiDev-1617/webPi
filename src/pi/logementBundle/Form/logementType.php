<?php

namespace pi\logementBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class logementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new StringToArrayTransformer();

        $builder ->add('description')
            ->add('typeChambre', ChoiceType::class, array(
                'choices'  => array(
                    'logement entier' => 'logement entier',
                    'chambre privée' => 'chambre privée',
                    'chambre partagée' => 'chambre partagée',
                )))
            ->add('nbVoyageur')
            ->add('typeHabitation', ChoiceType::class, array(
                'choices' => array(
                    'maison' => 'maison',
                    'appartement' => 'appartement',
                    'château' => 'château',
                    'bateau' => 'bateau',
                    'chalet' => 'chalet',
                    'bungalow' => 'bungalow',
                    'cabane' => 'cabane',
                    'tente' => 'tente',
                    'autre' => 'autre',
                )))
            ->add('nbLit')
            ->add('typeLit', ChoiceType::class, array(
                'choices' => array(
                    'vrai lit' => 'vrai lit',
                    'canapet convertible' => 'canapet convertible',
                    'canapet' => 'canapet',
                )))
            ->add('nbSallebain')
            ->add('typeSb', ChoiceType::class, array(
                'choices'  => array(
                    'Salle de bain privée' => 'Salle de bain privée',
                    'Salle de bain partagée' => 'Salle de bain partagée',
                )))
            ->add('equipement',ChoiceType::class, array(
                'choices' => array(
                    'Produits de base Serviettes' => 'Produits de base Serviettes',
                    'draps' => 'draps',
                    'savon et papier toilette' => 'savon et papier toilette',
                    'Wi-Fi' => 'Wi-Fi',
                    'Télévision' => 'Télévision',
                    'Chauffage' => 'Chauffage',
                    'Climatisation' => 'Climatisation',
                    'Cheminée' => 'Cheminée',
                    'Entrée privée' => 'Entrée privée',
                    'Bureau/Espace de travail' => 'Bureau/Espace de travail'),
                'expanded' => true,
                'multiple' => true
            ))
            ->add('espaceUtilise', ChoiceType::class, array(
                'choices' => array(
                    'Picine' => 'Picine',
                    'Parking' => 'Parking',
                    'Cuisine' => 'Cuisine',
                    'Salon Privé' => 'Salon Privé'),
                'expanded' => true,
                'multiple' => true
            ))
            ->add('prix', MoneyType::class)
            ->add('nbPoints', MoneyType::class)
            ->add('note')
            ->add('brochure',FileType::class,array(
                "label" => "photo ",
                "required" => FALSE,
                "data_class" => null,
                "attr" => array(
                    "multiple" => "multiple",)
            ))
            ->add('brochure1', FileType::class, array(
                "label" => "photo 1",
                "required" => FALSE,
                "data_class" => null,
                "attr" => array(
                    "multiple" => "multiple",)
            ))
            ->add('brochure2', FileType::class, array(
                "label" => "photo 2",
                "required" => FALSE,
                "data_class" => null,
                "attr" => array(
                    "multiple" => "multiple",)
            ))
            ->add('brochure3', FileType::class, array(
                "label" => "photo 3",
                "required" => FALSE,
                "data_class" => null,
                "attr" => array(
                    "multiple" => "multiple",)
            ))
            ->add('dateFin')
            ->add('titre')

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\logement'
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'pi_logementbundle_logement';
    }
}
