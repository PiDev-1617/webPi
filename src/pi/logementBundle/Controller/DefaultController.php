<?php

namespace pi\logementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('pilogementBundle:Default:index.html.twig');
    }
}
