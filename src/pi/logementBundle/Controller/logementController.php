<?php
namespace pi\logementBundle\Controller;

use AppBundle\Entity\logement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Logement controller.
 *
 */
class logementController extends Controller
{
    public function mesAnnonceAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $nb = $em->getRepository('AppBundle:logement')->findNbLogementbyid($id);
        $res = count($nb);
        $nbDate = $em->getRepository('AppBundle:logement')->findNbLogementbydate($id);
        $d = count($nbDate);
        if ($d != 0) {

            $this->get('session')->getFlashBag()->add(
                'notice',
                'La date limite d\'activation est dépassée, votre annonce est maintenant expirée.vous pouvez la renouveler ou l\'annuler pour acceder aux autres foncionalités du site .'
            );
            /**
             * @var $paginator \Knp\Component\Pager\Paginator
             */
            $paginator = $this->get('knp_paginator');
            $result = $paginator->paginate($nbDate, $request->query->getInt('page', 1),
                $request->query->getInt('limit', 6)
            );
            return $this->render('@pilogement/logement/mesLogement.html.twig', array(
                'logements' => $result,
            ));

        }
// die();
        if ($res == 0) {
            $this->get('session')->getFlashBag()->add(
                'notice',
                'vous n\'avez pas encore publié des annonces'
            );
            //$logements = $em->getRepository('AppBundle:logement')->findAll();
            $dql = "SELECT log FROM AppBundle:logement log ";
            $query = $em->createQuery($dql);
            /**
             * @var $paginator \Knp\Component\Pager\Paginator
             */
            $paginator = $this->get('knp_paginator');
            $result = $paginator->paginate($query, $request->query->getInt('page', 1),
                $request->query->getInt('limit', 6)
            );
            return $this->render('@pilogement/logement/index.html.twig', array(
                'logements' => $result,
            ));
    }
        $logements = $em->getRepository('AppBundle:logement')->findLogementbyid($id);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate($logements, $request->query->getInt('page', 1),
            $request->query->getInt('limit', 6)
        );
        return $this->render('@pilogement/logement/mesLogement.html.twig', array(
            'logements' => $result,
        ));
    }

    public function newAction(Request $request,$id)
    {
        $logement = new Logement();
        $em = $this->getDoctrine()->getManager();
        $User=$em->getRepository('piUserBundle:User')->find($id);
        $form = $this->createForm('pi\logementBundle\Form\logementType', $logement);
        $form->handleRequest($request);
        $nb = $em->getRepository('AppBundle:logement')->findNbLogementbyid($id);
        $res = count($nb);
        $nbDate = $em->getRepository('AppBundle:logement')->findNbLogementbydate($id);
        $d = count($nbDate);
        if ($d != 0) {
            /**
             * @var $paginator \Knp\Component\Pager\Paginator
             */
            $paginator = $this->get('knp_paginator');
            $result = $paginator->paginate($nbDate, $request->query->getInt('page', 1),
                $request->query->getInt('limit', 6)
            );
            return $this->render('@pilogement/logement/mesLogement.html.twig', array(
                'logements' => $result,
            ));

        }
        if ($res < 7) {

            if ($form->isSubmitted() && $form->isValid()) {
                // $file stores the uploaded PDF file
                $file = $logement->getBrochure();
                $file1 = $logement->getBrochure1();
                $file2 = $logement->getBrochure2();
                $file3 = $logement->getBrochure3();

                $Adressmap = $request->get('adressgmap');
                $LatLang = $request->get('latlng');
                $region = $request->get('region');
                $pays = $request->get('pays');
                $equip = $request->get('equipement');
                $espace = $request->get('espaceUtilise');

                // Generate a unique name for the file before saving it
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $fileName1 = md5(uniqid()) . '.' . $file1->guessExtension();
                $fileName2 = md5(uniqid()) . '.' . $file2->guessExtension();
                $fileName3 = md5(uniqid()) . '.' . $file3->guessExtension();

                // Move the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('brochures_directory'),
                    $fileName
                );
                $file1->move(
                    $this->getParameter('brochures_directory'),
                    $fileName1
                );
                $file2->move(
                    $this->getParameter('brochures_directory'),
                    $fileName2
                );
                $file3->move(
                    $this->getParameter('brochures_directory'),
                    $fileName3
                );
                // Update the 'brochure' property to store the PDF file name
                // instead of its contents
                $str = strlen($LatLang) - 2;
                $la = substr($LatLang, 1, $str);
                /* dump($la);

                 die();*/
                $logement->setBrochure($fileName);
                $logement->setBrochure1($fileName1);
                $logement->setBrochure2($fileName2);
                $logement->setBrochure3($fileName3);

                $logement->setLatlng($la);
                $logement->setAdressgmap($Adressmap);
                $logement->setRegion($region);
                $logement->setPays($pays);
                /*$logement->setLatlng($lang);*/
                $logement->setProprietaire($User);
                $em->persist($logement);
                $em->flush($logement);
                return $this->redirectToRoute('logement_show', array('id' => $logement->getIdLog()));
            }
            return $this->render('pilogementBundle:logement:new.html.twig', array(
                'logement' => $logement,
                'form' => $form->createView(),
            ));
        } else
            $this->get('session')->getFlashBag()->add(
                'notice',
                'vous avez depasser le nombre maximum des annonces'
            );
        return $this->indexAction($request);
    }

    /**
     * Lists all logement entities.
     *
     */
    public function indexAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();
            $mot = $request->get('motcle');
            $date = $request->get('date');
            $type = $request->get('guest');
            // dump($type);die();
            $logements = $em->getRepository('AppBundle:logement')->findalllogement();


            if ($mot != null && $date == null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDestianation($mot);
            }
            if ($mot == null && $date != null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDate($date);
            }
            if ($mot != null && $date != null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDateDest($date, $mot);
            }
            if ($mot == null && $date == null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByGuest($type);
            }
            if ($mot != null && $date != null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogement($date, $mot, $type);
            }
            if ($mot == null && $date == null && $type == '0') {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'veuillez sélectionner au moins un critère de recherche !!'
                );
            }
            /**
             * @var $paginator \Knp\Component\Pager\Paginator
             */
            $paginator = $this->get('knp_paginator');
            $result = $paginator->paginate($logements, $request->query->getInt('page', 1),
                $request->query->getInt('limit', 6)
            );
            return $this->render('@pilogement/logement/index.html.twig', array(
                'logements' => $result,
            ));
        }
        $em = $this->getDoctrine()->getManager();

        //$logements = $em->getRepository('AppBundle:logement')->findAll();
        $dql = "SELECT log FROM AppBundle:logement log ";
        $query = $em->createQuery($dql);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate($query, $request->query->getInt('page', 1),
            $request->query->getInt('limit', 6)
        );
        return $this->render('@pilogement/logement/index.html.twig', array(
            'logements' => $result,
        ));
    }

    /**
     * Finds and displays a logement entity.
     *
     */
    public function showAction(logement $logement)
    {
        return $this->render('@pilogement/logement/show.html.twig', array(
            'logement' => $logement,
        ));
    }
    /**
     * Displays a form to edit an existing logement entity.
     *
     */
    public function editAction(Request $request, logement $logement)
    {
        $editForm = $this->createForm('pi\logementBundle\Form\logementType', $logement);
        $editForm->handleRequest($request);
        $x = $logement->getAdressgmap();
        $y = $logement->getLatlng();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // $file stores the uploaded PDF file
            // Generate a unique name for the file before saving it
            $a = $request->get('adressgmap');
            $b = $request->get('latlng');

            $c = $request->get('region');
            $d = $request->get('pays');
            $file = $logement->getBrochure();
            $file1 = $logement->getBrochure1();
            $file2 = $logement->getBrochure2();
            $file3 = $logement->getBrochure3();
// Generate a unique name for the file before saving it
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $fileName1 = md5(uniqid()) . '.' . $file1->guessExtension();
            $fileName2 = md5(uniqid()) . '.' . $file2->guessExtension();
            $fileName3 = md5(uniqid()) . '.' . $file3->guessExtension();

            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('brochures_directory'),
                $fileName
            );
            $file1->move(
                $this->getParameter('brochures_directory'),
                $fileName1
            );
            $file2->move(
                $this->getParameter('brochures_directory'),
                $fileName2
            );
            $file3->move(
                $this->getParameter('brochures_directory'),
                $fileName3
            );

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents
            $str = strlen($b) - 2;
            $la = substr($b, 1, $str);
            $logement->setLatlng($la);
            $logement->setAdressgmap($a);
            $logement->setRegion($c);
            $logement->setPays($d);
            $logement->setBrochure($fileName);
            $logement->setBrochure1($fileName1);
            $logement->setBrochure2($fileName2);
            $logement->setBrochure3($fileName3);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('logement_show', array('id' => $logement->getIdLog()));
        }
        return $this->render('@pilogement/logement/edit.html.twig', array(
            'logement' => $logement,
            'edit_form' => $editForm->createView(), 'x' => $x, 'y' => $y
        ));
    }


    public function removeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $logement = $em->getRepository('AppBundle:logement')->find($id);
        $em->remove($logement);//enregistrer objet $model
        $em->flush();
        //executer req
        return $this->redirectToRoute('logement_index');
    }

    public function indexSCnxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //$logements = $em->getRepository('AppBundle:logement')->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result=  $paginator->paginate($query,$request->query->getInt('page',1),
            $request->query->getInt('limit',6)
        );
        return $this->render('@pilogement/logement/indexSanscnx.html.twig', array(
            'logements' => $result,
        ));
    }

    public function RchercheSCnxAction(Request $request)

    {
        if ($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $mot = $request->get('motcle');
            $date = $request->get('date');
            $type = $request->get('guest');
            $logements = $em->getRepository('AppBundle:logement')->findLogementByDate($date);
            if ($mot != null && $date == null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDestianation($mot);
            }
            if ($mot != null && $date == null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDestianation($mot);
            }
            if ($mot == null && $date != null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDestianation($mot);
            }
            if ($mot == null && $date != null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDate($date);
            }
            if ($mot != null && $date != null && $type == '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByDateDest($date, $mot);
            }
            if ($mot == null && $date == null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogementByGuest($type);
            }
            if ($mot != null && $date != null && $type != '0') {
                $logements = $em->getRepository('AppBundle:logement')->findLogement($date, $mot, $type);
            }
            if ($mot == null && $date == null && $type == '0') {
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'veuillez sélectionner au moins un critère de recherche !!'
                );
            }
            /**
             * @var $paginator \Knp\Component\Pager\Paginator
             */
            $paginator = $this->get('knp_paginator');
            $result = $paginator->paginate($logements, $request->query->getInt('page', 1),
                $request->query->getInt('limit', 6)
            );
            return $this->render('@pilogement/logement/indexSanscnx.html.twig', array(
                'logements' => $result,
            ));
        }
        $em = $this->getDoctrine()->getManager();

        //$logements = $em->getRepository('AppBundle:logement')->findAll();
        $dql = "SELECT log FROM AppBundle:logement log ";
        $query = $em->createQuery($dql);
        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate($query, $request->query->getInt('page', 1),
            $request->query->getInt('limit', 6)
        );
        return $this->render('@pilogement/logement/indexSanscnx.html.twig', array(
            'logements' => $result,
        ));
    }

}
