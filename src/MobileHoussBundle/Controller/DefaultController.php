<?php

namespace MobileHoussBundle\Controller;

use AppBundle\Entity\EspaceAprox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('MobileHoussBundle:Default:index.html.twig');
    }

    /**
     * @Route("/EspaceList")
     */
    public function ListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $espace = $em->getRepository('AppBundle:EspaceAprox')->findAll();

        $serializer = $this->container->get('serializer');
        $es = $serializer->serialize($espace, 'json');

        return new Response($es);
    }

    /**
     * @Route("/EspaceListrecherche")
     */
    public function rechercheeventAction()
    {
        $espace = new EspaceAprox();
        $em = $this->getDoctrine()->getManager();


        $espace = $em->getRepository('AppBundle:EspaceAprox')->findBy(array('addresse' => $espace->getAddresse()));

        $serializer = $this->container->get('serializer');
        $es = $serializer->serialize($espace, 'json');
        return new Response($es);

    }


}

