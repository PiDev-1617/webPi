<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new pi\piWebBundle\pipiWebBundle(),
            new pi\UserBundle\piUserBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new pi\logementBundle\pilogementBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new pi\EventBundle\piEventBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new \pi\EspaceBundle\piEspaceBundle(),
            new HostGuestBundle\HostGuestBundle(),
            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new ADesigns\CalendarBundle\ADesignsCalendarBundle(),
            new pi\GestionReclamationBundle\piGestionReclamationBundle(),
            new Nomaya\SocialBundle\NomayaSocialBundle(),
            new pi\StatBundle\piStatBundle(),
            new Ob\HighchartsBundle\ObHighchartsBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Vihuvac\Bundle\RecaptchaBundle\VihuvacRecaptchaBundle(),
            new MobileHoussBundle\MobileHoussBundle(),
            new Ras\Bundle\FlashAlertBundle\RasFlashAlertBundle(),
            new pi\FeedbackBundle\piFeedbackBundle(),
            new pi\SejourBundle\piSejourBundle(),
            new blackknight467\StarRatingBundle\StarRatingBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }

    public function getRootDir()
    {
        return __DIR__;
    }
}
